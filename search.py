# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

from game import Actions
import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def expand(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (child,
        action, stepCost), where 'child' is a child to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that child.
        """
        util.raiseNotDefined()

    def getActions(self, state):
        """
          state: Search state

        For a given state, this should return a list of possible actions.
        """
        util.raiseNotDefined()

    def getActionCost(self, state, action, next_state):
        """
          state: Search state
          action: action taken at state.
          next_state: next Search state after taking action.

        For a given state, this should return the cost of the (s, a, s') transition.
        """
        util.raiseNotDefined()

    def getNextState(self, state, action):
        """
          state: Search state
          action: action taken at state

        For a given state, this should return the next state after taking action from state.
        """
        util.raiseNotDefined()

    def getCostOfActionSequence(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    #La estructura utilizada es STACK
    frontera = util.Stack()
    #Lista que contendra los estados explorados
    Nodos_explorados = []

    #Se define el Estado de inicio
    StartState = problem.getStartState()
    StartNode = (StartState, [])

    frontera.push(StartNode)

    while not frontera.isEmpty():
        #Comienza a explorar el último nodo, el más reciente en la frontera
        StateActual, Actions = frontera.pop()

        if StateActual not in Nodos_explorados:
            #Se marca el estado actual como explorado
            Nodos_explorados.append(StateActual)
            #print(Nodos_explorados)

            if problem.isGoalState(StateActual):
                return Actions

            else:
                #Se obtiene una lista de posibles nodos sucesores de forma: (child, action, stepCost)
                Sucesor = problem.expand(StateActual)

                #hace un push de cada sucesor a la frontera
                for child, action, stepCost in Sucesor:
                    nextAction = Actions + [action]
                    newNodo = (child, nextAction)
                    frontera.push(newNodo)
    return Actions

    util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    #La estructura utilizada es Queue para hacer un FIFO
    frontera = util.Queue()
    #Lista que contendra los estados explorados
    Nodos_explorados = []

    #Se Define el estado Inicial
    StartState = problem.getStartState()
    StartNodo = (StartState, [], 0)

    frontera.push(StartNodo)

    while not frontera.isEmpty():
        #Comienza a explorar el primer nodo de la frontera, el más reciente.
        StateActual, Actions, CostoActual = frontera.pop()

        if StateActual not in Nodos_explorados:
            #Se marca el estado Actual como explorado 
            Nodos_explorados.append(StateActual)

            if problem.isGoalState(StateActual):
                return Actions
            else:
                #Se obtiene una lista de posibles nodos sucesores de forma: (child, action, stepCost)
                Sucesor = problem.expand(StateActual)

                #Hace un push por cada sucesor de la frontera actualizando la información
                for child, action, stepCost in Sucesor:
                    nextAction = Actions + [action]
                    newCost = CostoActual + stepCost
                    newNodo = (child, nextAction, newCost)

                    frontera.push(newNodo)
    return Actions
    util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    frontera = util.PriorityQueue()
    #Lista que contendra los estados explorados
    Nodos_explorados = []

    #Se Define el estado Inicial
    StartState = problem.getStartState()
    StartNodo = (StartState, [], 0)

    frontera.push(StartNodo, 0)

    while not frontera.isEmpty():

        #Comienza a explorar primero el nodo de menor costo combinado con la heuristica (cost+heuristic)
        StateActual, Actions, costoActual = frontera.pop()

        #Coloca el nodo explorado dentro de la lista
        NodoActual = (StateActual, costoActual)
        Nodos_explorados.append((StateActual, costoActual))

        if problem.isGoalState(StateActual):
            return Actions
        
        else:
            #Lista de (sucesor, accion y stepcost)
            sucesor = problem.expand(StateActual)

            #Por cada sucesor
            for child, action, stepcost in sucesor:
                newAction = Actions + [action]
                newCost = problem.getCostOfActionSequence(newAction)
                newNodo = (child, newAction, newCost)
                
                #Chequea si el sucesor ya fue explorado
                Explorado = False
                for exploraciones in Nodos_explorados:
                    #Examina cada nodo explorado
                    StateExplorado, CostExplorado = exploraciones

                    if (child == StateExplorado) and (newCost >= CostExplorado):
                        Explorado = True

                #si el sucesor no se explora, se coloca en la lista de frontera y en la lista de exploracion
                if not Explorado:
                    frontera.push(newNodo, newCost + heuristic(child, problem))
                    Nodos_explorados.append((child, newCost))
    return Actions
    util.raiseNotDefined()

def uniformCostSearch(problem):
    """Busca primero el nodo de menor costo total."""

    #Para ser explorados (FIFO): retiene (accion,costo)
    frontera = util.PriorityQueue()

    #Estados previamente explorados
    Nodos_explorados = {}

    StartState = problem.getStartState()
    StartNodo = (StartState, [], 0)
    print("Start: ", problem.isGoalState(StartState))

    frontera.push(StartNodo, 0)

    while not frontera.isEmpty():
        #Comienza la eploración con el menos costo de la frontera
        StateActual, Actions, costoActual = frontera.pop()

        if (StateActual not in Nodos_explorados) or (costoActual < Nodos_explorados[StateActual]):
            #Añade el estado del nodo dentro de la lista de exploración
            Nodos_explorados[StateActual] = costoActual

            if problem.isGoalState(StateActual):
                return Actions
            else:
                #Lista de (sucesor, accion, stepcost)
                sucesor = problem.expand(StateActual)

                for child, action, stepCost in sucesor:
                    newAction = Actions + [action]
                    newCost = costoActual + stepCost
                    newNodo = (child, newAction, newCost)

                    frontera.update(newNodo, newCost)
    return Actions
    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch