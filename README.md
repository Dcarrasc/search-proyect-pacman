# Search Proyect Pacman

## Tabla de información
***
1. [Información General](#Información-general)
2. [Tecnología](#tecnologías)
3. [Ejecución del programa](#Ejecución-del-programa)
4. [Colaboracion](#Colaboraciones)
5. [Documentación](#Documentacion)
    * [Problematica 1: Finding a Fixed Food Dot using Depth First Search](#Problema-1)
    * [Problematica 2: Finding a Fixed Food Dot using Breath-First Search](#Problema-2)
    * [Problematica 3: Finding a Fixed Food Dot using Astar Search](#Problema-3)
    * [Problematica 4: Finding All the Corners](#Problema-4)
6. [Modificación](#modificacion)
***

## Información General
En este Repositorio encontrará el Código que corresponde al desarrollo y soluciones de los problemas del 1 al 4 propuestos en la página:
`https://inst.eecs.berkeley.edu/~cs188/sp21/project1/`

También un video explicativo de las modificaciones que se realizarón y a que se diferencian los resultados con respecto al original.
## Tecnologías
***
* [Visual Studio Code](https://code.visualstudio.com) Version 1.61
* [GitLab](https://gitlab.com/Dcarrasc/search-proyect-pacman) Version 13.12.0
* [Python](https://www.python.org/downloads/) Version 3

## Ejecución del Programa
***
Para gestionar el programa utilizamos el recurso llamado "GitLab" que con algunos conocimientos de "GIT" puedes acceder facilmente.

Para poder correr el programa primero tendrás que estar dentro de una carpeta local y hacer un **Clone** desde el repositorio Git con el siguiente link:
```
$ git clone https://gitlab.com/Dcarrasc/search-proyect-pacman.git
```

Luego desde tu terminal acceder a la carpeta que acabas de **"Clonar"** en este casi se creará una carpeta con el nombre "search proyect pacman"

## Colaboraciones

Integrantes:
+ David Carrasco

## Documentacion
### Pacman
***
Una implementación en python de Algoritmos de búsqueda de inteligencia artificial para resolver problemas dentro del entorno Pac-man.

Se aplican conceptos de inteligencia artificial al clásico juego arcade. Ayudaremos a Pac-man a encontrar comida, evitar fantasmas y maximar su puntuación en el juego mediante una búsqueda en el espacio de estados desinformada e informado, la inferencia probabilística y el aprendizaje por refuerzo 

Este programa se compone de los siguientes archivos disponibles de forma original en Proyecto Base.

En la Rama de Primer_Algoritmo están los archivos que hemos modificado
+ La carpeta "**Search.py**" Para los problemas del 1 al 3.
+ La carpeta "**SearchAgents.py**" Para el problema 4.
+ La carpeta "**layouts**" Para la modificación de ambiente.

### Busqueda Desinformada
***
Para ver cómo le va a Pac-man usando algoritmos de búsqueda, podemos definir algunas variables:
```
python pacman.py -l MAZE_TYPE -p SearchAgent -a fn=SEARCH_ALGO
```
Donde `MAZE_TYPE` define el diseño del mapa y `SearchAgent` navega a Pac-man a través del laberinto de acuerdo con el algoritmo proporcionado en el parámetro `SEARCH_ALGO`

Dada las siguientes entradas:
* Estado Inicial - La celda del laberinto en la que comienza Pac-Man
* Funcion de Estado - Posibles acciones que puede realizar (por ejemplo moverse hacía el este)
* Estado del Objetivo - La celda en la que tiene que estar para ganar

El algoritmo de búsqueda devuelve una secuencia de estados que lleva a Pac-Man del estado inicial al objetivo. La estrategia de Busqueda es generar iterativamente una lista (llamada frontera) de estados sucesores hasta que encuentre una ruta al estado objetivo.

## Problema 1
Podemos comparar algunos algoritmos observando como le va a Pac-man en pequeño Laberinto `MAZE_TYPE` con un punto fijo de comida.

El estado inicial de Pac-Man está en la esquina superior derecha del laberinto. Su estado objetivo es una celda que contiene comida, que en este caso es la celda en la esquina inferior izquierda. 

Corriendo el siguiente codigo:
```
python pacman.py -l mediumMaze -p SearchAgent -a fn=dfs
```

<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/mediumMaze_DFS.PNG" title="mediumMaze_DFS"/>
</p>

**Estrategia:** La frontera de DFS es una cola LIFO (Last in, First out) el cual implementa un algoritmo de busqueda no informada utilizada para recorrer todos los estados de manera ordenada, pero no uniforme. Su funcionamiento consiste en ir expandiendo todos y cada uno de los nodos que va localizando en un camino concreto, cuando ya no quedan más acciones que realizar en dicho camino, regresa, de modo que repite el mismo proceso con cada uno de las acciones posibles del camino que se estaba procesando

## Problema 2
Corriendo el siguiente codigo:
```
python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs
```

<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/mediumMaze_BFS.PNG" title="mediumMaze_BFS" />
</p>

**Estrategia:** La frontera de BFS es una cola FIFO (First in, First out) y expande los sucesores en el orden en que se agregaron.

Comenzando en el estado inicial, BFS explora el camino hacia el oeste inicial, BFS explora el camino hacia el sur al mismo tiempo. BFS explora todas las celdas vecinas en cada ruta que están a 1 paso de distancia. Dado que ninguna es la celda objetivo, BFS intenta las celdas a 2 pasos de distancia y aún no hay celda objetivo, por lo que BFS intenta mirar a 3 pasos de distancia, y así sucesivamente. Hasta que BFS descubre el camino que produce comida, por lo que ese es el camino que toma Pac-man.

## Problema 3
Corriendo el siguiente codigo:
```
python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/bigMaze_ASTAR.PNG" title="bigMaze_ASTAR" />
</p>

**Estrategia:** El algoritmo de Astar, es un algoritmo de búsqueda de tipo informado. El algoritmo de A* encuentra, siempre y cuando se cumplan unas determinadas condiciones, el camino de menor coste entre un nodo origen y uno objetivo. Utiliza una función evalución de coste y de heuristica en cada estado.

#### Analisis
***
Probando con el Laberinto de OpenMaze podemos diferenciar como trabajan los distintos algoritmos, Para el Algoritmo de Astar se corre el siguiente comando:
```
python pacman.py -l OpenMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/OpenMaze_ASTAR.PNG" title="OpenMaze_ASTAR" />
</p>

Luego con el algoritmo de BFS, se corre el siguiente comando:
```
python pacman.py -l OpenMaze -z .5 -p SearchAgent -a fn=bfs
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/OpenMaze_BFS.PNG" title="OpenMaze_BFS" />
</p>

Luego con el algoritmo de DFS, se corre el siguiente comando:
```
python pacman.py -l OpenMaze -z .5 -p SearchAgent -a fn=dfs
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/OpenMaze_DFS.PNG" title="OpenMaze_DFS" />
</p>

### Analisis
Como podemos ver en las imagenes, el mejor algoritmo de búsqueda para el "OpenMaze" es el agoritmo A*, ya que encuentra el camino más optimo siendo este con un costo de 54.
El algoritmo bfs tambien se acerca a la solución más optima a excepción de que ocupa más memoria para encontrar la solución.

## Problema 4

**Estrategia:** Para resolver este problema el Pac-man tiene que recorrer las esquinas independiente de si hay comida o no, para esto puede utilizar distintos tipos de Algoritmos de búsqueda donde cada uno operara de igual manera que las explicaciones anteriores.
Corriendo el siguiente codigo:
```
python pacman.py -l tinyCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/tinyCorners_BFS.PNG" title="tinyCorners_BFS" />
</p>

Luego para el algoritmo de "BFS" se coloca el siguiente comando:
```
python pacman.py -l mediumCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/mediumCorners_BFS.PNG" title="mediumCorners_BFS" />
</p>

Luego para el algoritmo de "DFS" se coloca el siguiente comando:
```
python pacman.py -l mediumCorners -p SearchAgent -a fn=dfs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/mediumCorners_DFS.PNG" title="mediumCorners_DFS" />
</p>

Finalmente una modificación es el algoritmo "UCS" conocido como UniformCostSearch utilizando el mismo problema con el siguiente comando:

```
python pacman.py -l mediumCorners -p SearchAgent -a fn=ucs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/mediumCorners_UCS.PNG" title="mediumCorners_UCS" />
</p>

### Analisis
***
Probando con el mapa de "mediumCorners" podemos diferenciar como trabajan los distintos algoritmos. El camino más optimo será el camino con menor costo, esto no siempre sucede ya que también hay que considerar la memoria que ocupa en recorrer los caminos, siendo este un recurso importante a considerar. 

Haciendo una comparación entre "BFS" vs "DFS", se puede ver claramente que el algoritmo de búsqueda "BFS" es más optimo que "DFS" aunque sacrificando memoria.

Luego agregando un nuevo algoritmo de busqueda el cual sería "UCS" (Uniform Cost Search) En costo y en Nodos expandidos es igual a "BFS", teniendo una leve diferencia, la cual sería el tiempo de computación, siendo este 0.1 seg.

## Modificacion

<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/Demostraci%C3%B3n_Modificaci%C3%B3n.mp4" />
</p>


Una modificación que me plantee para trabajar, es un laberinto con las siglas de la carrera telematica. "TEL" ocupando la problematica de recorrer las esquinas, podremos ver como trabaja cada algoritmo con este nuevo laberinto.

Para poder verlo se tiene que correr con el siguiente comando:
```
python pacman.py -l Telematics -p SearchAgent -a fn=bfs,prob=CornersProblem
python pacman.py -l Telematics -p SearchAgent -a fn=dfs,prob=CornersProblem
python pacman.py -l Telematics -p SearchAgent -a fn=ucs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/Telematics_BFS.PNG" title="Telematics_BFS" />
</p>
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/Telematics_DFS.PNG" title="Telematics_DFS" />
</p>
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/Telematics_UCS.PNG" title="Telematics_UCS" />
</p>

#### Analisis

En las imagenes de arriba se puede comprobar como se desarrolla cada algoritmo con sus respectivos costos y exploraciones. Dada la facilidad del laberinto el costo y la exploración es igual para 2 de los algoritmos siendo la diferencia entre ellos el tiempo de computación en procesar cada algoritmo.

Ordenandolos en un Top del más optimo al menos optimo quedaría de la siguiente manera:

+ UCS (Uniform Cost Search) 
+ BFS (Breath First Search) 
+ DFS (Depth First Search) 

## Modificación Dificil

Para poder verlo se tiene que correr con el siguiente comando:
```
python pacman.py -l TelematicsV2 -p SearchAgent -a fn=bfs,prob=CornersProblem
python pacman.py -l TelematicsV2 -p SearchAgent -a fn=dfs,prob=CornersProblem
python pacman.py -l TelematicsV2 -p SearchAgent -a fn=ucs,prob=CornersProblem
```
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/TelematicsV2_BFS.PNG" title="TelematicsV2_BFS" />
</p>
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/TelematicsV2_DFS.PNG" title="TelematicsV2_DFS" />
</p>
<p align="center">
<img src="https://gitlab.com/Dcarrasc/search-proyect-pacman/-/raw/main/Fotos/TelematicsV2_UCS.PNG" title="TelematicsV2_UCS" />
</p>

En las imagenes de arriba se puede comprobar como se desarrolla cada algoritmo con sus respectivos costos y exploraciones. Ahora la dificultad del laberinto a cambiado y se puede notar en los nuevos costos para encontrar la solución. 

Ordenandolos en un nuevo Top del más optimo al menos optimo:
+ UCS (Uniform Cost Search) 
+ BFS (Breath First Search) 
+ DFS (Depth First Search) 

Podemos notar que se mantiene en los mismos puesto que el laberinto más fácil y la diferencia entre el primer y segundo puesto es nuevamente el tiempo de computación de ambos.